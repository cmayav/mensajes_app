package com.platzi.mensajes_app;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Mensajes {

	int id_mensaje;
	String mensaje;
	String autor_mensaje;
	String fecha_mensaje;
}
