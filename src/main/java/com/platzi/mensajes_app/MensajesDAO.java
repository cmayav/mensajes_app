package com.platzi.mensajes_app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MensajesDAO {

	public static void crearMensajeDB(Mensajes mensaje) {
		Conexion dbConnect = new Conexion();

		try {

			Connection conexion = dbConnect.get_connection();
			PreparedStatement ps = null;

			String query = "INSERT INTO mensajes (mensaje, autor_mensaje) VALUES (?,?)";
			ps = conexion.prepareStatement(query);
			ps.setString(1, mensaje.getMensaje());
			ps.setString(2, mensaje.getAutor_mensaje());
			ps.executeUpdate();
			System.out.println("Mensaje creado");

		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	public static void leerMensajesDB() {

		Conexion dbConnect = new Conexion();

		try {

			Connection conexion = dbConnect.get_connection();
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "SELECT * FROM mensajes";
			ps = conexion.prepareStatement(query);
			rs = ps.executeQuery();

			while (rs.next()) {
				System.out.println("id " + rs.getInt("id_mensaje"));
				System.out.println("mensaje " + rs.getString("mensaje"));
				System.out.println("Autor " + rs.getString("autor_mensaje"));
				System.out.println("fecha " + rs.getString("fecha_mensaje"));
			}

		} catch (SQLException e) {
			System.out.println("No hay mensajes... Error");
		}
	}

	public static void borrarMensajeDB(int id_mensaje) {

		Conexion dbConnect = new Conexion();

		try {

			Connection conexion = dbConnect.get_connection();
			PreparedStatement ps = null;
			ResultSet rs = null;

			String query = "DELETE FROM mensajes WHERE id_mensaje = ?";
			ps = conexion.prepareStatement(query);
			ps.setInt(1, id_mensaje);
			ps.executeUpdate();
			System.out.println("El mensaje ha sido borrado");

		} catch (SQLException e) {
			System.out.println("... Error");
		}

	}

	public static void actualizarMensajeDB(Mensajes mensaje) {
		Conexion dbConnect = new Conexion();

		try {

			Connection conexion = dbConnect.get_connection();
			PreparedStatement ps = null;

			String query = "UPDATE mensajes SET mensaje = ? WHERE id_mensaje = ?";
			ps = conexion.prepareStatement(query);
			ps.setString(1, mensaje.getMensaje());
			ps.setInt(2, mensaje.getId_mensaje());
			
			ps.executeUpdate();
			System.out.println("El mensaje ha sido actualizado");

		} catch (SQLException e) {
			System.out.println("... Error");
		}
	}

}
