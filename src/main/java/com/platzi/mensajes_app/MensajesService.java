package com.platzi.mensajes_app;

import java.util.Scanner;

public class MensajesService {

	public static void crearMensaje() {

		Scanner sc = new Scanner(System.in);
		System.out.println("Escribe tu mensaje: ");
		String msj = sc.nextLine();

		System.out.println("Autor: ");
		String autor = sc.nextLine();

		Mensajes registro = new Mensajes();
		registro.setMensaje(msj);
		registro.setAutor_mensaje(autor);

		MensajesDAO.crearMensajeDB(registro);

	}

	public static void listarMensajes() {

		MensajesDAO.leerMensajesDB();

	}

	public static void borrarMensaje() {
		Scanner sc = new Scanner(System.in);
		System.out.println("id de mensaje a borrar: ");
		int id_msj = sc.nextInt();
		
		MensajesDAO.borrarMensajeDB(id_msj);
	}

	public static void editarMensaje() {
		Scanner sc = new Scanner(System.in);
		System.out.println("nuevo mensaje: ");
		String msj = sc.nextLine();
		System.out.println("id de mensaje a editar: ");
		int id_msj = sc.nextInt();
		
		Mensajes registro = new Mensajes();
		registro.setId_mensaje(id_msj);
		registro.setMensaje(msj);
		System.out.println("Creo el objeto");
		MensajesDAO.actualizarMensajeDB(registro);
	}

}
